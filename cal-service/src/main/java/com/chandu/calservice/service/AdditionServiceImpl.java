package com.chandu.calservice.service;

import org.springframework.stereotype.Service;

@Service
public class AdditionServiceImpl implements AdditionService{

	@Override
	public int add(int x, int y) {
		return x + y;
	}

}
