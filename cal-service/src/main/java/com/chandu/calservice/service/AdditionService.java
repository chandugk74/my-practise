package com.chandu.calservice.service;

public interface AdditionService {

	int add(int x, int y);

}
