package com.chandu.calservice.service;

import org.springframework.stereotype.Service;

@Service
public class DivisionServiceImpl implements DivisionService {

	@Override
	public int div(int x, int y) {
		return x/y;
	}

}
