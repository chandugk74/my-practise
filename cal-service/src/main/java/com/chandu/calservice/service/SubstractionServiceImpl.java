package com.chandu.calservice.service;

import org.springframework.stereotype.Service;

@Service
public class SubstractionServiceImpl implements SubstractionService {

	@Override
	public int sub(int x, int y) {
		return x - y;
	}

}
