package com.chandu.calservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.chandu.calservice.service.AdditionService;
import com.chandu.calservice.service.DivisionService;
import com.chandu.calservice.service.SubstractionService;

@RestController
@RequestMapping("/api/calservice")
public class CalController {

	@Autowired
	AdditionService additionService;

	@Autowired
	SubstractionService substractionService;
	
	@Autowired
	DivisionService divisionService;

	@GetMapping("/cal")
	public ResponseEntity<String> get() {
		ResponseEntity<String> obj = new ResponseEntity<String>("This is cal Service", HttpStatus.OK);
		return obj;

	}

	@PostMapping("/sum")
	public ResponseEntity<Integer> sum(@RequestParam("m") int m, @RequestParam("n") int n) {
		int sum = additionService.add(m, n);
		ResponseEntity<Integer> obj1 = new ResponseEntity<Integer>(sum, HttpStatus.OK);
		return obj1;
	}

	@PostMapping("/sub")
	public ResponseEntity<Integer> sub(@RequestParam("m") int m, @RequestParam("n") int n) {
		int sub = substractionService.sub(m, n);
		ResponseEntity<Integer> obj1 = new ResponseEntity<Integer>(sub, HttpStatus.OK);
		return obj1;
	}
	@PostMapping("/div")
	public ResponseEntity<Integer> div(@RequestParam("m") int m, @RequestParam("n") int n) {
		int sub = divisionService.div(m, n);
		ResponseEntity<Integer> obj1 = new ResponseEntity<Integer>(sub, HttpStatus.OK);
		return obj1;
	}
}
