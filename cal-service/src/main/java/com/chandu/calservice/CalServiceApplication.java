package com.chandu.calservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CalServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CalServiceApplication.class, args);
	}

}
